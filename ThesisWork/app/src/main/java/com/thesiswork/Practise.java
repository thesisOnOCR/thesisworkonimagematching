package com.thesiswork;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Practise extends GraphicsActivity {
	private Paint  mPaint;
	private DisplayMetrics dm;
	private Bitmap bm;
	private Rect src;
	private int practiseType;
	private int letterResourceId;
	private int letterCount=0;
	LinearLayout paintLayout;
	MyView mv;	
	private Bitmap  mBitmap;
	private Bitmap  aBitmap;
	AlertDialog alert;
	private Canvas  aCanvas;
	Display display;
	int myHeight = 0;

	private Integer[] shorborno={
			R.drawable.aw, 
			R.drawable.a, 
			R.drawable.i, 
			R.drawable.ee, 
			R.drawable.u, 
			R.drawable.oo, 
			R.drawable.ri, 
			R.drawable.e, 
			R.drawable.oy, 
			R.drawable.o, 
			R.drawable.ou};

	private Integer[] banjonborno={
			R.drawable.kaw,
			R.drawable.khaw,
			R.drawable.gaw, 
			R.drawable.ghaw,
			R.drawable.e_nasal,
			R.drawable.chaw,
			R.drawable.chchaw,
			R.drawable.jaw,
			R.drawable.jhaw,
			R.drawable.o_nasal, 
			R.drawable.cerebral_taw,
			R.drawable.cerebral_thaw,
			R.drawable.cerebral_daw, 
			R.drawable.cerebral_dhaw,
			R.drawable.mordhonno_naw, 
			R.drawable.dental_taw,
			R.drawable.dental_thaw,
			R.drawable.dental_daw,
			R.drawable.dental_dhaw,
			R.drawable.dental_naw, 
			R.drawable.paw, 
			R.drawable.phaw, 
			R.drawable.baw,
			R.drawable.bhaw,
			R.drawable.maw, 
			R.drawable.antostho_jaw, 
			R.drawable.bolshno_raw, 
			R.drawable.law,
			R.drawable.talobbo_shaw,
			R.drawable.mordhonno_shaw,
			R.drawable.dento_shaw,
			R.drawable.haw,			
			R.drawable.daeshonno_raw,			 
			R.drawable.dhaeshonno_raw, 			
			R.drawable.antostho_aw, 
			R.drawable.chondro_taw, 
			R.drawable.oonekshur,
			R.drawable.bishargo, 
			R.drawable.chandra_bindoo, 
	};

	private Integer[] shonkha={R.drawable.ek,R.drawable.dui,R.drawable.tin,R.drawable.charr, R.drawable.pach,			 
			R.drawable.choi, R.drawable.sat, R.drawable.at,R.drawable.noi,R.drawable.dosh};

	//Dialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.practise);


		/*//set up dialog
		dialog = new Dialog(Practise.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.customdialog);
		        
		//set up button
		Button button = (Button) dialog.findViewById(R.id.cancelButton);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		Button button2 = (Button) dialog.findViewById(R.id.buyButton);
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d("sadat","buy true");
				dialog.dismiss();
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=com.banglaDroid.banglaAlphabet"));
				startActivity(intent);

			}
		});
		//set up text
		TextView text = (TextView) dialog.findViewById(R.id.TextView01);
		text.setText("All the features are not available in this free version. Please Buy the Pro Version.");
		*/

		practiseType=(int)getIntent().getIntExtra("PRACTISETYPE", 1);
		switch(practiseType){
		case Alphabet.BANJONBORNO: 
			letterResourceId=banjonborno[0];
			break;
		case Alphabet.SHORBORNO:
			letterResourceId=shorborno[0];
			break;
		case Alphabet.NUMBERS:
			letterResourceId=shonkha[0];
			break;
		}

		dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setColor(0xFFFF0000);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		int in_dp = 15;  // 10 dps
		final float scale = getResources().getDisplayMetrics().density;
		int in_px = (int) (in_dp * scale + 0.5f);
		mPaint.setStrokeWidth(in_px);		

		display = getWindowManager().getDefaultDisplay();
		switch (dm.densityDpi) {
		case DisplayMetrics.DENSITY_HIGH:
			Log.i("display", "high");
			myHeight = display.getHeight() - 38*2;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			Log.i("display", "medium/default");
			myHeight = display.getHeight() - 25*2;
			break;
		case DisplayMetrics.DENSITY_LOW:
			Log.i("display", "low");
			myHeight = display.getHeight() - 19*2;
			break;
		default:
			myHeight = display.getHeight() - 24*2;
			Log.i("display", "Unknown density");
		}

		paintLayout=(LinearLayout)findViewById(R.id.paintLayout);
		mv=new MyView(this);
		mv.setDrawingCacheEnabled(false);
		paintLayout.addView(mv);

		//aCanvas.drawColor(Color.WHITE);

	}

	public void colorChanged(int color) {
		mPaint.setColor(color);
	}

	public class MyView extends View {		
		private Canvas  mCanvas;		
		private Path    mPath;
		private Paint   mBitmapPaint;



		public void loadChar(){
			bm=BitmapFactory.decodeResource(getResources(), letterResourceId);			
			src=new Rect(0,0,bm.getWidth(),bm.getHeight());
			Rect dest=new Rect(0,0,(int)(display.getWidth()*.75),display.getHeight());

			aBitmap = Bitmap.createBitmap((int)(display.getWidth()*.75),display.getHeight(), Bitmap.Config.ARGB_8888);
			aCanvas=new Canvas(aBitmap);

			mBitmap = Bitmap.createBitmap((int)(display.getWidth()*.75),display.getHeight(), Bitmap.Config.ARGB_8888);									
			mCanvas = new Canvas(mBitmap);				
			mCanvas.drawBitmap(bm, src, dest,mPaint);				


			mPath = new Path();
			mBitmapPaint = new Paint(Paint.DITHER_FLAG);
		}


		public void loadBlank(){
			bm=BitmapFactory.decodeResource(getResources(), letterResourceId);			
			src=new Rect(0,0,bm.getWidth(),bm.getHeight());
			//Rect dest=new Rect(0,0,(int)(display.getWidth()*.75),myHeight);

			aBitmap = Bitmap.createBitmap((int)(display.getWidth()*.75),display.getHeight(), Bitmap.Config.ARGB_8888);
			aCanvas=new Canvas(aBitmap);

			mBitmap = Bitmap.createBitmap((int)(display.getWidth()*.75),display.getHeight(), Bitmap.Config.ARGB_8888);									
			mCanvas = new Canvas(mBitmap);				
			//mCanvas.drawBitmap(bm, src, dest,mPaint);				


			mPath = new Path();
			mBitmapPaint = new Paint(Paint.DITHER_FLAG);
		}

		public MyView(Context c) {
			super(c);						
			loadChar();
			//mBitmapPaint.setAlpha(255);
		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			super.onSizeChanged(w, h, oldw, oldh);
		}

		@Override
		protected void onDraw(Canvas canvas) {
			canvas.drawColor(0xFFAAAAAA);
			canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
			canvas.drawBitmap(aBitmap, 0, 0, mBitmapPaint);
			canvas.drawPath(mPath, mPaint);
		}

		private float mX, mY;
		private static final float TOUCH_TOLERANCE = 4;

		private void touch_start(float x, float y) {
			mPath.reset();
			mPath.moveTo(x, y);
			mX = x;
			mY = y;
		}
		private void touch_move(float x, float y) {
			float dx = Math.abs(x - mX);
			float dy = Math.abs(y - mY);
			if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
				mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
				mX = x;
				mY = y;
			}
		}
		private void touch_up() {
			mPath.lineTo(mX, mY);
			// commit the path to our offscreen
			aCanvas.drawPath(mPath, mPaint);
			mCanvas.drawPath(mPath, mPaint);

			// kill this so we don't double draw
			mPath.reset();
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			float x = event.getX();
			float y = event.getY();

			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				touch_start(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				touch_move(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_UP:
				touch_up();
				invalidate();
				break;
			}
			return true;
		}
	}


	public void chooseColor(View v){
		final CharSequence[] colors = {"Red", "Green", "Blue"};		
		AlertDialog.Builder alt_bld = new AlertDialog.Builder (this);		
		alt_bld.setTitle("Select a Color");
		alt_bld.setSingleChoiceItems(colors, -1, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				switch(item){
				case 0:
					colorChanged(Color.RED);
					break;
				case 1:
					colorChanged(Color.GREEN);
					break;
				case 2:
					colorChanged(Color.BLUE);
					break;
				}
				alert.dismiss();
			}
		});
		alert = alt_bld.create();		
		alert.show();

	}

	public void resetCharacter(){
		mv.loadChar();
		mv.invalidate();
	}

	private boolean blankButton=false;
	public void blank(View v){
		blankButton=true;
		mv.loadBlank();
		mv.invalidate();
	}

	public void resetCharacter(View v){
		if(blankButton==true){
			mv.loadBlank();
			mv.invalidate();
		}else{
			mv.loadChar();
			mv.invalidate();
		}
	}

	public void prevClick(View v){
		blankButton=false;
		switch(practiseType){
		case Alphabet.SHORBORNO:
			if(letterCount>0){
				letterCount--;
				letterResourceId=shorborno[letterCount];
				resetCharacter();
				Log.d("sadat",""+letterCount);
			}
			break;
		case Alphabet.BANJONBORNO:
			if(letterCount>0){
				letterCount--;
				letterResourceId=banjonborno[letterCount];
				resetCharacter();
				Log.d("sadat",""+letterCount);
			}
			break;
		case Alphabet.NUMBERS:
			if(letterCount>0){
				letterCount--;
				letterResourceId=shonkha[letterCount];
				resetCharacter();
				Log.d("sadat",""+letterCount);
			}
			break;
		}
	}

	public void nextClick(View v){
		if(letterCount>5){
			//Toast.makeText(Practise.this, "Please Buy the Pro Version.", Toast.LENGTH_SHORT).show();
			showDialog(0);
		}else{
			blankButton=false;
			switch(practiseType){
			case Alphabet.SHORBORNO:
				if(letterCount<shorborno.length-1){
					letterCount++;
					letterResourceId=shorborno[letterCount];
					resetCharacter();
					Log.d("sadat",""+letterCount);
				}
				break;
			case Alphabet.BANJONBORNO:
				if(letterCount<banjonborno.length-1){
					letterCount++;
					letterResourceId=banjonborno[letterCount];
					resetCharacter();
					Log.d("sadat",""+letterCount);
				}
				break;
			case Alphabet.NUMBERS:
				if(letterCount<shonkha.length-1){
					letterCount++;
					letterResourceId=shonkha[letterCount];
					resetCharacter();
					Log.d("sadat",""+letterCount);
				}
				break;
			}
		}
	}

	public void saveCharacter(View v){
		Handler saveHandler = new Handler(){  
			@Override  
			public void handleMessage(Message msg) {  
				Log.d("sadat",""+msg.arg2);
				Toast.makeText(Practise.this, "Your Character has been saved", Toast.LENGTH_LONG).show();
			}  
		} ;  
		new ExportBitmapToFile(this,saveHandler, aBitmap).execute("test");  
	}

	private class ExportBitmapToFile extends AsyncTask<String, Void, Boolean> {
		private Context mContext;  
		private Handler mHandler;  
		private Bitmap nBitmap;  


		public ExportBitmapToFile(Context context,Handler handler,Bitmap bitmap) {  
			mContext = context;  
			nBitmap = bitmap;  
			mHandler = handler;  
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {  
				/*if (!APP_FILE_PATH.exists()) {  
	    	          APP_FILE_PATH.mkdirs();  
	    	        } */ 
				Random r=new Random();
				File root = Environment.getExternalStorageDirectory();
				final FileOutputStream out = new FileOutputStream(new File(root,"MyCharacter"+Math.abs(r.nextInt())+".png"));  
				nBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);  
				out.flush();  
				out.close();  
				return true;  
			}catch (Exception e) { 	    	    	  
				Log.d("sadat",e.getMessage());
				e.printStackTrace();  
			}  
			//mHandler.post(completeRunnable);  
			return false; 
		}

		@Override  
		protected void onPostExecute(Boolean bool) {  
			super.onPostExecute(bool);  
			if ( bool ){  
				mHandler.sendEmptyMessage(1);
			}  
		}		
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		LayoutInflater factory = LayoutInflater.from(this);
		final View textProMessage = factory.inflate(R.layout.pro_message, null);        
		return new AlertDialog.Builder(Practise.this)
		.setIcon(R.drawable.icon)
		.setTitle("Get the Full Version")
		.setView(textProMessage)
		.setPositiveButton("Buy", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=com.banglaDroid.banglaAlphabet"));
				startActivity(intent);
			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				//finish();
			}
		})
		.create();
	}
}
